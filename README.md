# Visible Demo

A Demo App that showcases the use of Adobe Target through Tealium IQ and the Tealium RemoteCommand API.

## Download or Clone

You can either download and unzip the project from the icon in the top left or clone the repo.

### Clone

```
git clone https://cmsund@bitbucket.org/cmsund/visible-demo.git
```

### Installing

You might need to run `pod update` in your root folder to build successfully.


## Authors

* **Christina Sund** - [GitHub Tealium](https://github.com/christinatealium) - [GitHub Personal](https://github.com/cmsund) - [BitBucket](https://bitbucket.org/cmsund/)

