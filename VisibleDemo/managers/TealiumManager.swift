//
//  TealiumManager.swift
//  VisibleDemo
//
//  Created by Christina Sund on 10/26/18.
//  Copyright © 2018 Christina. All rights reserved.
//

import Foundation
import tealium_swift

extension String: Error {}

/// Example of a shared helper to handle all 3rd party tracking services. This
/// paradigm is recommended to reduce burden of future code updates for external services
/// in general.
@objc class TealiumManager: NSObject {

    static let shared = TealiumManager()
    var tealium: Tealium?
    var enableHelperLogs = true
    var signInRemoteCommandAdded: TealiumRemoteCommand?
    var affinityRemoteCommandAdded: TealiumRemoteCommand?
    static var targetData = TargetData()
    typealias AffinityCompletionBlock = () -> Void

    override public init() {
        super.init()
    }

    @objc func start() {
        // REQUIRED Config object for lib
        let config = TealiumConfig(account: "services-christina",
                                   profile: "targetmobile",
                                   environment: "prod",
                                   datasource: "test12",
                                   optionalData: nil)

        // OPTIONALLY set log level
        config.setLogLevel(logLevel: .verbose)
        // OPTIONALLY add an external delegate
        config.addDelegate(self)
        // OPTIONALLY disable a particular module by name
        let list = TealiumModulesList(isWhitelist: false,
                                      moduleNames: ["collect", "consentmanager"])
        config.setModulesList(list)
        // REQUIRED Initialization
        tealium = Tealium(config: config, completion: { (_) in
                            // Optional processing post init.
                            // OPTIONALLY implement Dynamic Triggers/Remote Commands.
                            #if os(iOS)
                            let remoteCommand = TealiumRemoteCommand(commandId: "logger",
                                                                     description: "test") { (response) in
                                                                        if TealiumManager.shared.enableHelperLogs {
                                                                            print(response)
                                                                        }
                            }
                            // this must be done inside the Tealium init callback,
                            // otherwise remotecommands won't be avaialable
                            if let remoteCommands = self.tealium?.remoteCommands() {
                                remoteCommands.add(remoteCommand)
                            } else {
                                return
                            }
                            #endif
        })
        // example showing persistent data
        self.tealium?.persistentData()?.add(data: ["testPersistentKey": "testPersistentValue"])
        // example showing volatile data
        self.tealium?.volatileData()?.add(data: ["testVolatileKey": "testVolatileValue"])
        // process a tracking call on the background queue
        // example tracking call - not required in production
        DispatchQueue.global(qos: .background).async {
            self.tealium?.track(title: "HelperReady_BG_Queue")
        }
        self.tealium?.track(title: "HelperReady")
    }
    func trackAppearance(_ viewController: UIViewController) {
        if let accessibilityIdentifier = viewController.view.accessibilityIdentifier {
            track(title: accessibilityIdentifier, data: nil)
            return
        }
        if let title = viewController.title {
            track(title: title, data: nil)
        }
    }
    // track an event
    @objc func track(title: String, data: [String: Any]?) {
        tealium?.track(title: title,
                       data: data,
                       completion: { (success, info, error) in
                        // Optional post processing
                        if self.enableHelperLogs == false {
                            return
                        }
                        print("""
                        *** TealiumManager: track completed:
                        \n\(success)\n\(String(describing: info))\n\(String(describing: error))
                        """)
        })
    }

    // track a screen view
    @objc func trackView(title: String, data: [String: Any]?) {
        tealium?.trackView(title: title,
                           data: data,
                           completion: { (success, info, error) in
                            if self.enableHelperLogs == false {
                                return
                            }
                            print("""
                            *** TealiumManager: view completed:
                            \n\(success)\n\(String(describing: info))\n\(String(describing: error))
                            """)
        })
    }

    @objc func crash() {
        NSException.raise(NSExceptionName(rawValue: "Exception"),
                          format: "This is a test exception",
                          arguments: getVaList(["nil"]))
    }
}

extension TealiumManager: TealiumDelegate {

    func tealiumShouldTrack(data: [String: Any]) -> Bool {
        if signInRemoteCommandAdded == nil {
            signInRemoteCommandAdded = TealiumRemoteCommand(commandId: "display_signin",
                                                            description: "Display Experience Data") { (response) in
                let customCommandData = response.payload()
                guard let converted = ConvertKeys.init(fromDictionary: customCommandData) else {
                    assertionFailure("Error converting keys in Custom Command Data")
                    return
                }
                TealiumManager.targetData.experience = converted.experience
            }
            TealiumManager.shared.tealium?.remoteCommands()?.add(signInRemoteCommandAdded!)
        }
        return true
    }

    func tealiumTrackCompleted(success: Bool, info: [String: Any]?, error: Error?) {
        if enableHelperLogs == false {
            return
        }
        print("""
            \n*** Tealium Helper: Tealium Delegate : tealiumTrackCompleted ***
            Track finished. Was successful:\(success)\nInfo:
            \(info as AnyObject)\((error != nil) ? "\nError:\(String(describing: error))":"")
            """)
    }

    func enableDisplayTargetAffinity(completion: @escaping AffinityCompletionBlock) {
        if affinityRemoteCommandAdded == nil {
            affinityRemoteCommandAdded = TealiumRemoteCommand(commandId: "display_affinity",
                                                              description: "Display Experience Data") { (response) in
                let customCommandData = response.payload()
                guard let content = customCommandData["content"] as? String else {
                    print("Payload does not contain 'content'")
                    return
                }
                if (content.components(separatedBy: "content\":\"{").count > 1) {
                    TealiumManager.targetData.experience = content.components(separatedBy: "content\":\"{")[1]
                        .components(separatedBy: "\"experience\":\"")[1]
                        .components(separatedBy: "\"}\"")[0]
                    completion()
                } else {
                    TealiumManager.targetData.experience = "control"
                    completion()
                }
            }
            tealium?.remoteCommands()?.add(affinityRemoteCommandAdded!)
        }
    }

}
