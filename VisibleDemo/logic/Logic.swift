//
//  Logic.swift
//  VisibleDemo
//
//  Created by Christina on 10/30/18.
//  Copyright © 2018 Christina. All rights reserved.
//

import UIKit

class Logic {

    class func run(rootViewController: VisibleViewController) {
        let tasks = [
            Logic.configure(rootViewController),
            Logic.presentLogin(rootViewController)
        ]
        tasks.runAsyncTasks(loops: 1, initialState: nil, completion: nil)
    }

    class func configure(_ viewController: VisibleViewController) -> AsyncTask {
        return { (session: Any?, taskCompletion: @escaping AsyncTaskCompletion) in
            TealiumManager.shared.tealium?.trackView(title: "loading_screen",
                                                     data: nil,
                                                     completion: { (_, _, error) in
                if error != nil {
                    // Handle error
                } else {
                    taskCompletion(session)
                }
            })
        }
    }

    class func presentLogin(_ viewController: VisibleViewController) -> AsyncTask {
        return { (session: Any?, taskCompletion: @escaping AsyncTaskCompletion) in
            viewController.performSegue(withIdentifier: "present_signin", sender: nil)
        }
    }
}
