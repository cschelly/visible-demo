//
//  TargetExperiences.swift
//  VisibleDemo
//
//  Created by Christina on 11/30/18.
//  Copyright © 2018 Christina. All rights reserved.
//

import Foundation

enum TargetExperiences {
    static var expA = "A"
    static var expB = "B"
    static var expC = "C"
    static var expD = "D"
    static var expE = "E"
    static var expF = "F"
}
