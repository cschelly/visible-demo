//
//  TargetData.swift
//  VisibleDemo
//
//  Created by Christina on 11/27/18.
//  Copyright © 2018 Christina. All rights reserved.
//

import Foundation

struct TargetData {
    var title: String = ""
    var mbox: String = ""
    var experience: String = ""
    var location: String = ""
    var marketingCloudVisitorId = ""

    init() {

    }

    init(title: String, mbox: String, experience: String, location: String, marketingCloudVisitorId: String) {
        self.title = title
        self.mbox = mbox
        self.experience = experience
        self.location = location
        self.marketingCloudVisitorId = marketingCloudVisitorId
    }
}
