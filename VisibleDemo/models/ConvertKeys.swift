//
//  ConvertKeys.swift
//  VisibleDemo
//
//  Created by Christina on 11/27/18.
//  Copyright © 2018 Christina. All rights reserved.
//

import Foundation

class ConvertKeys {

    var title: String!
    var debug: String!
    var mbox: String!
    var command_id: String!
    var experience: String!
    var location: String!
    var tntId: String!
    var marketingCloudVisitorId: String!
    var edgeHost: String!
    var thirdPartyId: String!
    var sessionId: String!

    init?(fromDictionary dictionary: [String: Any]) {
        title = dictionary["title"] as? String
        debug = dictionary["debug"] as? String
        mbox = dictionary["mbox"] as? String
        command_id = dictionary["command_id"] as? String
        experience = dictionary["Experience"] as? String
        location = dictionary["Loc"] as? String
        tntId = dictionary["tntId"] as? String
        marketingCloudVisitorId = dictionary["marketingCloudVisitorId"] as? String
        edgeHost = dictionary["edgeHost"] as? String
        thirdPartyId = dictionary["thirdPartyId"] as? String
        sessionId = dictionary["sessionId"] as? String
    }
}
