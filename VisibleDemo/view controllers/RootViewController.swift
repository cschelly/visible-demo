//
//  RootViewController.swift
//  VisibleDemo
//
//  Created by Christina on 10/30/18.
//  Copyright © 2018 Christina. All rights reserved.
//

import UIKit

class RootViewController: VisibleViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Logic.run(rootViewController: self)
    }

}
