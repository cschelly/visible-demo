//
//  SecondViewController.swift
//  VisibleDemo
//
//  Created by Christina Sund on 10/26/18.
//  Copyright © 2018 Christina. All rights reserved.
//

import UIKit

class Affinity: UIViewController {

    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var salesLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        image.image = nil
        salesLabel.text = ""
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        TealiumManager.shared.trackView(title: "dog_affinity_view", data: nil)
        TealiumManager.shared.enableDisplayTargetAffinity {
            self.setupExperience()
        }
    }

    private func setupExperience() {
        switch TealiumManager.targetData.experience.uppercased() {
        case TargetExperiences.expB:
            image.image = UIImage(named: "dog.jpg")
            salesLabel.text = """
            Hey! Someone told us you like dogs, here is a cute picture for you to enjoy.
            You can also view more dog pictures and videos like this on your phone for less
            by upgrading your plan. Call 1-888-555-9999 today!
            """
        case TargetExperiences.expC:
            image.image = UIImage(named: "cat.jpg")
            salesLabel.text = """
            Meow there, someone told us you like cats meow. Here is a cute picture for you to
            enjoy meow. You can also view more cat pictures and videos like this on your phone
            for less by upgrading your plan meow. Call 1-888-555-1111 moew!
            """
        default:
            print("Control experience: \(TealiumManager.targetData.experience)")
            image.image = UIImage(named: "control.png")
            salesLabel.text = """
            Greetings! Are you enjoying your free trial? You can continue to have unlimited data,
            messages,and minutes for $40/mo. Call 1-888-555-9999 to upgrade your plan!
            """
        }
    }

}
