//
//  VisibleViewController.swift
//  VisibleDemo
//
//  Created by Christina on 10/30/18.
//  Copyright © 2018 Christina. All rights reserved.
//

import UIKit

class VisibleViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
}
