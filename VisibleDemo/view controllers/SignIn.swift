//
//  FirstViewController.swift
//  VisibleDemo
//
//  Created by Christina Sund on 10/26/18.
//  Copyright © 2018 Christina. All rights reserved.
//

import UIKit

class SignIn: UIViewController {

    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var signInButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTextFields()
        setupButton(button: signInButton, title: "Sign In")
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        TealiumManager.shared.trackView(title: "signin_view", data: nil)
    }

    private func setupTextFields() {
        email.textColor = UIColor(hexString: "#505359")
        email.backgroundColor = UIColor.dim()
        email.placeholder = "Email"
        email.autocorrectionType = .no
        password.textColor = UIColor(hexString: "#505359")
        password.backgroundColor = UIColor.dim()
        password.placeholder = "Password"
        password.isSecureTextEntry = true
        password.autocorrectionType = .no
    }

    private func setupButton(button: UIButton, title: String?) {
        if let title = title {
            button.setTitle(title, for: .normal)
        }
        switch TealiumManager.targetData.experience {
        case TargetExperiences.expA:
            signInButton.setTitleColor(UIColor(hexString: "#1700FF"), for: .normal)
            signInButton.backgroundColor = .white
        case TargetExperiences.expB:
           signInButton.setTitleColor(.white, for: .normal)
            signInButton.backgroundColor = UIColor(hexString: "#1700FF")
        case TargetExperiences.expC:
            signInButton.setTitleColor(.green, for: .normal)
            signInButton.backgroundColor = .blue
        case TargetExperiences.expD:
            signInButton.setTitleColor(UIColor(hexString: "#1700FF"), for: .normal)
            signInButton.backgroundColor = .green
        case TargetExperiences.expE:
            signInButton.setTitleColor(UIColor(hexString: "#FFFFFF"), for: .normal)
            signInButton.backgroundColor = .purple
        case TargetExperiences.expF:
            signInButton.setTitleColor(UIColor(hexString: "#000000"), for: .normal)
            signInButton.backgroundColor = .orange
        default:
            signInButton.setTitleColor(.white, for: .normal)
            signInButton.backgroundColor = UIColor(hexString: "#1700FF")
        }

    }

}
